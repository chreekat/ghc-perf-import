{ nixpkgs ? import <nixpkgs> {}, compiler ? "default", doBenchmark ? false }:

let

  inherit (nixpkgs) pkgs;

  haskellPackages = if compiler == "default"
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler};

  variant = if doBenchmark then pkgs.haskell.lib.doBenchmark else pkgs.lib.id;

  lens-regex-pcre = haskellPackages.callHackage "lens-regex-pcre" "0.3.1.0" {};

  drv = variant (haskellPackages.callCabal2nix "perf-import" ./. { inherit lens-regex-pcre; });

in

  if pkgs.lib.inNixShell then drv.env else drv
