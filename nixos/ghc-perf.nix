{ accessToken }:
{ pkgs, lib, ... }:

let
  dbName = "ghc_perf";
  dbWebUser = "ghc_perf_web";
  serviceUser = "ghc_perf";
  postgrestUser = "ghc_perf_web";
  postgrestPort = 8889;

  ghc-perf-import = pkgs.haskellPackages.callCabal2nix "perf-import" ../import {};

  ghc-perf-web = pkgs.callPackage (import ../web.nix) {};

  gitlab-bot = pkgs.callPackage ../gitlab-bot {};

  postgrest = import ./postgrest { inherit pkgs; };
  postgrestConfig = builtins.toFile "postgrest.conf" ''
    db-uri = "postgres://${dbWebUser}@/${dbName}"
    db-schema = "public"
    db-anon-role = "${dbWebUser}"
    db-pool = 10
  
    server-host = "*4"
    server-port = ${toString postgrestPort}
  '';

  pre-import-script = ''
    if ! ${pkgs.sudo}/bin/sudo -upostgres ${pkgs.postgresql}/bin/psql -lqtA | grep -q "^${dbName}|"; then
      echo "Initializing schema..."
      ${pkgs.sudo}/bin/sudo -upostgres ${pkgs.postgresql}/bin/psql \
        -c "CREATE ROLE ghc_perf WITH LOGIN;" \
        -c "CREATE DATABASE ${dbName} WITH OWNER ghc_perf;"
      ${pkgs.sudo}/bin/sudo -ughc_perf ${pkgs.postgresql}/bin/psql -U ghc_perf < ${../import/schema.sql}
      ${pkgs.sudo}/bin/sudo -upostgres ${pkgs.postgresql}/bin/psql \
        -c "GRANT SELECT ON ALL TABLES IN SCHEMA public TO ghc_perf_web;"
      echo "done."
    fi
  '';

  import-script = ''
    cd /var/cache/ghc-perf
    if [ ! -d ghc ]; then
      echo "cloning $(pwd)/ghc..."
      git clone https://gitlab.haskell.org/ghc/ghc
    fi
    cd ghc

    echo "updating $(pwd)/ghc..."
    git pull
    git fetch https://gitlab.haskell.org/ghc/ghc-performance-notes.git refs/notes/perf:refs/notes/ci/perf

    echo "importing commits..."
    perf-import-git -c postgresql:///${dbName} -d ghc master

    echo "importing notes..."
    perf-import-notes -c postgresql:///${dbName} -d ghc -R refs/notes/ci/perf
  '';

in {

  imports = [ ../alerts/nixos.nix ];

  users.users."${serviceUser}" = {
    description = "User for ghc-perf import script";
    isSystemUser = true;
    group = serviceUser;
  };
  users.groups."${serviceUser}" = {};

  systemd.services.ghc-perf-gitlab-import-bot = {
    description = "ghc-perf metric import bot";
    preStart = pre-import-script;
    script = ''
      ghc-perf-import-service \
        --gitlab-root=https://gitlab.haskell.org/ \
        --access-token=${accessToken} \
        --conn-string=postgresql:///ghc_perf \
        --port=7088
    '';
    path = [ pkgs.git gitlab-bot ghc-perf-import ];
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      User = "${serviceUser}";
      PermissionsStartOnly = true;
      CacheDirectory = "ghc-perf";
    };
  };

  systemd.services.ghc-note-perf-import = {
    description = "Update ghc-perf metrics from perf notes";
    preStart = pre-import-script;
    script = import-script;
    path = [ pkgs.git ghc-perf-import ];
    serviceConfig = {
      User = "${serviceUser}";
      PermissionsStartOnly = true;
      CacheDirectory = "ghc-perf";
    };
  };

  systemd.timers.ghc-note-perf-import = {
    description = "Periodically update ghc-perf metrics";
    wants = [ "network.target" ];
    wantedBy = [ "multi-user.target" ];
    timerConfig = {
      OnCalendar = "*-*-* 3:00:00";
      Unit = "ghc-note-perf-import.service";
    };
  };

  users.users."${postgrestUser}" = {
    isSystemUser = true;
    shell = "/bin/false";
    group = postgrestUser;
  };
  users.groups."${postgrestUser}" = {};

  systemd.services.postgrest-ghc-perf = {
    description = "Postgrest instance for ghc-perf";
    script = ''
      ${postgrest}/bin/postgrest ${postgrestConfig}
    '';
    wantedBy = [ "multi-user.target" ];
    serviceConfig.User = "${postgrestUser}";
  };

}

